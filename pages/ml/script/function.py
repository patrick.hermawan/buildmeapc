import re
import numpy as np
import urllib.request
import json
import nltk
import spacy


# ========================================
# Common
# ========================================


# Make a "dictionary" list of words
def unique(list1, index):
    for x in list1:
        if x not in index:
            index.append(x)
    return index


class json_encoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__

# ========================================
# download.py
# ========================================

# Man Got Class, but man can never be hot
class Build(object):
    CPU = ''
    RAM = ''
    SSD = 0
    HDD = 0
    GPU = ''
    budget = 0
    request = ''

    def __init__(self, CPU, RAM, SSD, HDD, GPU, budget, request):
        self.CPU = CPU
        self.RAM = RAM
        self.SSD = SSD
        self.HDD = HDD
        self.GPU = GPU
        self.budget = budget
        self.request = request

    def __repr__(self):
        return '\n\nCPU: ' + str(self.CPU) + '\nRAM : ' + str(self.RAM) + '\nSSD : ' + str(self.SSD) + '\nHDD : ' + str(self.HDD) + '\nGPU : ' + str(self.GPU) + '\nBudget : ' + str(self.budget) + '\nRequest : ' + str(self.request)


def make_build(CPU,RAM,SSD,HDD,GPU,budget,request):
    build = Build(CPU, RAM, SSD, HDD, GPU, budget, request)
    return build


# Comment class that contains the comment body (text) and score
class Comment(object):
    body = ''
    score = 0

    def __init__(self, score, body):
        self.body = body
        self.score = score

    def __repr__(self):
        return '\n \n Score: ' + str(self.score) + '\nBody : ' + str(self.body)


def make_comment(score,body):
    comment = Comment(score,body)
    return comment


def get_score(comment):
    return comment.score


# Return a string between "this string" and "that string"
def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""





# Gather all the direct-child comments from a comment/submission, and call itself for its child comments, if exist.
# The end result is an array of Comment class (comment contains text and score), all within one level
# no specific order
def gather_child_comments(parent):
    comments = []
    children = []
    if hasattr(parent, 'comments'):
        children = parent.comments
    if hasattr(parent, 'replies'):
        children = parent.replies
    for child in children:
        if hasattr(child, 'score'):
            comments.append(make_comment(child.score, child.body))
            comments = comments + gather_child_comments(child)
    return comments


# Find the first thing that appears in the entire comments
# def find_first_thing(submission,betweenThis,andThis):
#     output = ""
#     comments = gather_child_comments(submission)
#     comments.sort(key=get_score, reverse=True)
#
#     for comment in comments:
#         output = find_between(comment.body,betweenThis,andThis)
#         if output != "":
#             break
#     return output


# Find the first thing that appears in the entire comments
def find_top_build(submission):
    output = ""
    comments = gather_child_comments(submission)
    comments.sort(key=get_score, reverse=True)

    for comment in comments:
        output1 = find_between(comment.body,'**Total** | **', '**')
        output2 = find_between(comment.body,'**CPU** | [','](')
        output3 = find_between(comment.body,'**Memory** | [','](')
        output4 = find_between(comment.body,'**Storage** | [',' Internal Hard Drive](')
        output5 = find_between(comment.body,'**Storage** | [',' Solid State Drive](')
        if (not not output1) & (not not output2) & (not not output3) & ((not not output4) | (not not output5)):
            output = comment.body
            break
    return output


# Dev use only. Creates a file containing ALL the words (dictionary) that are not in the ignore list
# def dev_create_ignore_list(all_new_words):
#     old_to_ignore = np.genfromtxt('ignore.txt', dtype='str')
#
#     for word in old_to_ignore:
#         if word in all_new_words:
#             all_new_words.remove(word)
#     np.savetxt('ignore_save.txt', index_words, fmt='%s')


# Gather the currency rates from X to USD, and saves it into a variable with its name. e.g. AUD = 1.5000
def gather_currency_rates():
    for currency in ['AUD', 'EUR', 'CAD', 'INR', 'NZD', 'SEK', 'GBP']:
        with urllib.request.urlopen("http://free.currencyconverterapi.com/api/v5/convert?q=" + currency + "_USD&compact=y") as url:
            data = json.loads(url.read().decode())
            globals()[currency] = data[currency + '_USD']['val']


# Convert a string of currency ($100.00) to a USD number-only
def convert_to_usd(string, loc):
    first_digit = re.search("\d", string).start() # Address of the first digit
    value = find_between( string[first_digit:], '', '.' )
    value = re.sub("[^0-9]", "", value)
    if not value:
        return 0
    value = int(value)
    if not loc:
        return value
    if loc in ['be', 'de', 'es' , 'fr' , 'ie' , 'it' ,'nl']:
        return value * EUR
    if loc == 'ca':
        return value * CAD
    if loc == 'uk':
        return value * GBP
    if loc == 'au':
        return value * AUD
    if loc == 'in':
        return value * INR
    if loc == 'nz':
        return value * NZD
    if loc == 'se':
        return value * SEK
    else:
        return 0

#
# # Index (dictionary) needed, convert each item in the list into a one-hot array
# def prepare_for_y(array, index):
#     i = 0
#     for cpu in array:
#         matrix = np.zeros(shape=(len(index),1))
#         j = 0
#         for ref in index:
#             if cpu == ref:
#                 matrix[j] = 1
#                 break
#             j = j+1
#         array[i] = matrix
#         i = i+1
#     return array


def remove_questions(submission):
    submission = submission.replace(
        'What will you be doing with this PC','')
    submission = submission.replace(
        'Be as specific as possible, and include specific games or programs you will be using','')
    submission = submission.replace(
        'What is your maximum budget before rebates/shipping/taxes','')
    submission = submission.replace(
        'When do you plan on building/buying the PC','')
    submission = submission.replace(
        'Note: beyond a week or two from today means any build you receive will be out of date when you want to buy','')
    submission = submission.replace(
        'What, exactly, do you need included in the budget','')
    submission = submission.replace(
        'Tower/OS/monitor/keyboard/mouse/etc','')
    submission = submission.replace(
        'Which country (and state/province) will you be purchasing the parts in','')
    submission = submission.replace(
        "If you're in US, do you have access to a Microcenter location",'')
    submission = submission.replace(
        'If reusing any parts (including monitor(s)/keyboard/mouse/etc), what parts will you be reusing? Brands and models are appreciated','')
    submission = submission.replace(
        'Will you be overclocking? If yes, are you interested in overclocking right away, or down the line? CPU and/or GPU?','')
    submission = submission.replace(
        'Are there any specific features or items you want/need in the build','')
    submission = submission.replace(
        'ex: SSD, large amount of storage or a RAID setup, CUDA or OpenCL support, etc','')
    submission = submission.replace(
        'Do you have any specific case preferences (Size like ITX/microATX/mid-tower/full-tower, styles, colors, window or not, LED lighting, etc)','')
    submission = submission.replace(
        'or a particular color theme preference for the components','')
    submission = submission.replace(
        'Do you need a copy of Windows included in the budget','')
    submission = submission.replace(
        'If you do need one included, do you have a preference','')
    submission = submission.replace(
        'Extra info or particulars','')
    submission = submission.lower()
    for char in ['\n', '*', '>', ':', '(',')','[',']','{','}',"\\"]:
        submission = submission.replace(char,'')
    submission = submission.replace('?','.')

    return submission


# The input must contain something like "10GB", "10 GB", "1TB", or "1 TB". The output is just an int in gigabyte
def get_gigabyte(str):
    gb = 0
    str = str.split()
    for idx, word in enumerate(str):
        if word == "GB":
            gb = int(str[idx-1])
            break
        if word == "TB":
            gb = int(str[idx-1])*1000
            break
        if "GB" in word:
            gb = re.sub("[^0-9]", "", word)
            if not gb:
                return 0
            gb = int(gb)
            break
        if "TB" in word:
            gb = re.sub("[^0-9]", "", word)
            if not gb:
                return 0
            gb = int(gb) * 1000
            break
    return gb

# ==================================================
# process.py
# ==================================================


# MAN GOT CLASS. Man can never be hot
class MySentences(object):
    def __init__(self, sentences):
        self.sentences = sentences

    def __iter__(self):
        for sentence in self.sentences:
            yield sentence.split()


# Dev use only. Creates a file containing ALL the words (Dictionary) that are not in the ignore list
# def dev_create_ignore_list(all_new_words):
#     old_to_ignore = np.genfromtxt('ignore.txt', dtype='str')
#
#     for word in old_to_ignore:
#         if word in all_new_words:
#             all_new_words.remove(word)
#     np.savetxt('ignore_save.txt', index_words, fmt='%s')


# Index (Dictionary) needed, convert each item in the list into a one-hot array
def one_hot(array, index):
    i = 0
    for cpu in array:
        matrix = np.zeros(shape=(len(index), 1))
        j = 0
        for ref in index:
            if cpu == ref:
                matrix[j] = 1
                break
            j = j + 1
        array[i] = matrix
        i = i + 1
    return array

def one_hot_index(cpu, index):
    j = 0
    for ref in index:
        if cpu == ref:
            break
        j = j + 1
    return j

# def one_hot_index(array, index):
#     i = 0
#     output = []
#     for cpu in array:
#         j = 0
#         for ref in index:
#             if cpu == ref:
#                 output.append(j)
#                 break
#             j = j + 1
#         i = i + 1
#     return output

def prepare_nltk():
    nltk.download('wordnet')
    nltk.download('stopwords')

# Inspiration: https://towardsdatascience.com/topic-modelling-in-python-with-nltk-and-gensim-4ef03213cd21
def tokenize(text):
    spacy.load('en')
    parser = spacy.lang.en.English()
    
    lda_tokens = []
    tokens = parser(text)
    for token in tokens:
        if token.orth_.isspace():
            continue
        elif token.like_url:
            lda_tokens.append('URL')
        elif token.orth_.startswith('@'):
            lda_tokens.append('SCREEN_NAME')
        else:
            lda_tokens.append(token.lower_)
    return lda_tokens


def prepare_text_for_lda(text):
    en_stop = set(nltk.corpus.stopwords.words('english'))
    tokens = tokenize(text)
    tokens = [token for token in tokens if len(token) > 4]
    tokens = [token for token in tokens if token not in en_stop]
    tokens = [get_lemma(token) for token in tokens]
    return tokens


def get_lemma(word):
    lemma = nltk.corpus.wordnet.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma


# def get_lemma2(word):
#     return WordNetLemmatizer().lemmatize(word)


    # Spoiler alert: Gonna be used below
def unroll(list_of_lists):
    output = []
    for lst in list_of_lists:
        for item in lst:
            output.append(item)
    return output
