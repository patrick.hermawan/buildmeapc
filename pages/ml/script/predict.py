import numpy as np
import json
import gensim
import os
from .function import prepare_text_for_lda
from .function import unroll
from .function import make_build
from .function import json_encoder
import re
import tensorflow as tf

#filename: data/181109_buildapcforme_month_model

def predict(filename, budget, original_request): # filename is index
    data_folder = os.path.join("pages", "ml", "data")
    model_folder = os.path.join("pages", "ml", "model")

    filename = os.path.join(data_folder, filename)
    with open(filename, 'r') as filehandle:
        index = json.load(filehandle) # load the index

    # request = remove_questions(request)
    request = prepare_text_for_lda(original_request)

    last_number = len(request)
    for i in range(last_number, 1000):
        request.append("<PAD>")

    request2 = []

    model = gensim.models.Word2Vec.load(re.sub("_index.json", "", filename) + "_word2vec.model")
    for word in request:
        request2.append(model[word].tolist())
    request = request2

    x = []
    x.append(float(budget))
    x = x + unroll(request)
    x = np.asarray([x])

    # files = os.listdir('data')
    # to_remove = []
    # for file in files:
    #     if 'index' not in file:
    #         to_remove.append(file)
    # for file in to_remove:
    #     files.remove(file)


    # filename = re.sub("data", "model", filename)
    filename = re.sub("index.json", "", filename) # filename is now model/181113_buildapcforme_month_
    filename = os.path.split(filename)[-1]

    files = os.listdir(model_folder) # list all files in model folder
    architectures = [] # the JSON model file
    for file in files:
        if (filename in file) & (file.endswith('_architecture.json')): # Filter only the _architecture that matches the index date
            architectures.append(file)

    for architecture in architectures: # this should be repeated 5 times
        with open(os.path.join(model_folder,architecture), 'r') as filehandle:
            architecture_json = json.load(filehandle)  # load the index

        architecture_json = json.dumps(architecture_json)
        model = tf.keras.models.model_from_json(architecture_json)

        filename = re.sub("_architecture.json", "", architecture)
        filename = filename + "_weights"
        files = os.listdir(model_folder)
        to_remove = []
        for file in files:
            if filename not in file:
                to_remove.append(file)
        for file in to_remove:
            files.remove(file)

        model.load_weights(os.path.join(model_folder,files[-1]))

        pred = model.predict(x)

        for component in index:
            # print(type(index[component]))
            if component in architecture:
                if isinstance(index[component], list):
                    m = max(pred[0])
                    m = [i for i, j in enumerate(pred[0]) if j == m]
                    m = m[-1]
                    m = index[component][m]
                    globals()[component] = str(m)
                else:
                    globals()[component] = int(pred[0])

    builds = []
    build = make_build(CPU, RAM, SSD, HDD, GPU, budget, original_request)
    builds.append(build)

    builds = json_encoder().encode(builds)


    # build = json_encoder().encode(build)

    return builds

    # x_length = x.shape[1]
    #
    # try:
    #     index_length = len(index[component])
    # except:
    #     index_length = 1




    # m = max(pred[0])
    # m = [i for i, j in enumerate(pred[0]) if j == m]
    # m = m[0]
    # recommendation.append("Recommended " + component + ": "+ index[component][m])

#
# print("Budget: " + str(to_predict[0]["budget"]))
# print("Request: " + str(to_predict[0]["request"]))
# for item in recommendation:
#     print(item)

    # for idx, file in enumerate(files):
    #     file = re.sub(filename[5:] + "_", "", file)
    #     file = re.sub(".ckpt", "", file)
    #     files[idx] = file
    #
    # components_to_predict = files
    # print(components_to_predict)

    # with open("test.json", 'r') as filehandle:
    #     to_predict = json.load(filehandle)

    # budget = to_predict[0]["budget"]
    # request = to_predict[0]["request"]






