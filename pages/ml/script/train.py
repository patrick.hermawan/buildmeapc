import time
import numpy as np
import random
import json
import re
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['TF_CPP_MIN_VLOG_LEVEL'] = '3'
import tensorflow as tf
import matplotlib.pyplot as plt
from .model import create_model


def train(filename,component):
    # Because I have an ass GPU
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.per_process_gpu_memory_fraction = 0.75
    tf.keras.backend.set_session(tf.Session(config=config))

    filename_x = filename
    filename_y = re.sub("_trainx.json", "_trainy.json", filename)
    filename_index = re.sub("_trainx.json", "_index.json", filename)
    print("Loading the processed training file: " + filename_x)
    with open(filename_x, 'r') as filehandle:
        builds_x = json.load(filehandle)
    print("Loading the processed training file: " + filename_y)
    with open(filename_y, 'r') as filehandle:
        builds_y = json.load(filehandle)

    print("Loading the processed INDEX file: " + filename_index)
    with open(filename_index, 'r') as filehandle:
        index = json.load(filehandle)

    if component not in index:
        print("Sorry not supported yet") # TODO: Add support for regression (HDD, SSD)
        return False

    train_x = []
    train_y = []
    validation_x = []
    validation_y = []

    for i in range(0, len(builds_x)):
        if random.uniform(1, 10) > 8.5: # approximately 15% of the data is used for validation
            validation_x.append(builds_x[i])
            validation_y.append(builds_y[component][i])
        else: # the rest of the data is used for training
            train_x.append(builds_x[i])
            train_y.append(builds_y[component][i])

    train_x = np.asarray(train_x)
    train_y = np.asarray(train_y)
    validation_x = np.asarray(validation_x)
    validation_y = np.asarray(validation_y)


    print("Length of Validation")
    print(len(validation_x))
    print(validation_x.shape)
    print("Length of Validation 0")
    print(len(validation_x[0]))
    print(validation_x[0].shape)
    print("Length of Train")
    print(len(train_x))
    print("Length of Train 0")
    print(len(train_x[0]))
    print("Type of Train 0")
    print(type(train_x[0][0]))
    print("Length of TrainY")
    print(len(train_y))
    print("Type of TrainY 0")
    print(type(train_y[0]))


    x_length = len(train_x[0])

    try:
        index_length = len(index[component])
    except:
        index_length = 1

    with open("config.json", 'r') as filehandle:
        config = json.load(filehandle)

    if component in config['train']:
        learning_rates = config['train'][component]['learning_rates']
        optimizers = config['train'][component]['optimizers']
        epochs = config['train'][component]['epochs']
        nodes = config['train'][component]['nodes']
        batch_size = config['train'][component]['batch_size']
    else:
        learning_rates = config['train']['general']['learning_rates']
        optimizers = config['train']['general']['optimizers']
        epochs = config['train']['general']['epochs']
        nodes = config['train']['general']['nodes']
        batch_size = config['train']['general']['batch_size']

    for optimizer in optimizers:
        for learning_rate in learning_rates:
            for node in nodes:
                tf.keras.backend.clear_session()

                model_filename = re.sub("data", "model", filename)
                model_filename = re.sub("trainx.json", "", model_filename) + component + "_" + optimizer + "_" + str(learning_rate) + '_' + str(node)

                if (os.path.isfile(model_filename + '_weights.h5')):
                    print("Weights file already exists, skipping")
                    continue

                model = create_model(x_length, index_length, node, learning_rate, optimizer)
                model.summary()

                with open(model_filename + '_architecture.json', 'w') as f:
                    f.write(model.to_json())

                # t0 = time.time()
                history = model.fit(train_x, train_y,
                                    validation_data=(validation_x, validation_y),
                                    epochs=epochs,
                                    batch_size=batch_size,
                                    verbose=True,
                                    # callbacks=[WeightsSaver(model, 1000)]
                                    callbacks=[
                                        tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0,
                                                                         patience=1000, verbose=1, mode='auto',
                                                                         baseline=None),
                                        tf.keras.callbacks.ReduceLROnPlateau(monitor='val_mean_absolute_error', factor=0.1,
                                                                             patience=10, verbose=1, mode='auto',
                                                                             min_delta=0.0001, cooldown=0, min_lr=0),
                                        tf.keras.callbacks.ModelCheckpoint(model_filename + '_weights{epoch:05d}.h5',
                                                                           save_weights_only=True, period=((epochs / 5)+1)),
                                    ]
                                    )
                # t1 = time.time()
                # print("=====================")
                # print("Time is:" + str(t1-t0))
                # print("=====================")

                model.save_weights(model_filename + '_weights.h5')

                plt.figure()
                plt.xlabel('Epoch')
                plt.ylabel('Mean Abs Error [1000$]')

                train_loss = 0
                val_loss = 0
                if 'mean_absolute_error' in history.history:
                    train_loss = history.history['mean_absolute_error']
                    val_loss = history.history['val_mean_absolute_error']
                elif 'loss' in history.history:
                    train_loss = history.history['loss']
                    val_loss = history.history['val_loss']

                plt.plot(history.epoch, np.array(train_loss), label='Loss in Train Data')
                plt.plot(history.epoch, np.array(val_loss), label='Loss in Validation Data')
                plt.legend()
                plt.title(re.sub("model", "", model_filename))
                plt.savefig(model_filename + '.png')
