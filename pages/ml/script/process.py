import json
from .function import unique
from .function import one_hot_index
from .function import prepare_text_for_lda
from .function import json_encoder
from .function import unroll
from .function import prepare_nltk
import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
import gensim
import re


def process_file(filename):
    prepare_nltk()
    print("Loading the downloaded raw file: " + filename)
    with open(filename, 'r') as filehandle:
        builds = json.load(filehandle)

    # CREATING DICTIONARY / INDEX
    index = {}

    print("Getting rid of the spaces before GB")
    for idx, build in enumerate(builds):
        builds[idx]['GPU'] = re.sub(" GB", "GB", builds[idx]['GPU'])
        builds[idx]['CPU'] = re.sub(" GHz", "GHz", builds[idx]['CPU'])

    def process_index(component):
        print("Creating index for " + component + '...')
        if isinstance(builds[0][component], int):
            index[component] = True
        else:
            index[component] = []
            for build in builds:
                index[component] = unique([build[component]],index[component])
            index[component].sort()
            for idx, build in enumerate(builds):
                builds[idx][component] = one_hot_index(builds[idx][component],index[component])

    for component in builds[0]:
        if not ((component == 'budget') | (component == 'request')):
            process_index(component)


    print("Saving the indexes")
    index = json_encoder().encode(index)

    with open(re.sub("_raw.json", "_index.json", filename), "w") as text_file:
        text_file.write(index)
        text_file.close()
    print("Indexes saved as: " + re.sub("_raw.json", "_index.json", filename))

    requests = []

    print("Preparing the requests by managing the wording")
    for idx, build in enumerate(builds):
        request = prepare_text_for_lda(build['request'])
        last_number = len(request)
        for i in range(last_number, 1000):
            request.append("<PAD>")
        builds[idx]['request'] = request
        requests.append(request)

    print("Modeling the words in the request into a Word2Vec model")
    model = gensim.models.Word2Vec(requests, min_count=1, size=10)
    model.save(re.sub("_raw.json", "_word2vec.model", filename))
    print("Word2vec model saved as: " + re.sub("_raw.json", "_word2vec.model", filename))

    for idx, build in enumerate(builds):
        request = []
        for word in build['request']:
            request.append(model[word].tolist())
        builds[idx]['request'] = request

    x = []
    y = {}

    #TODO: Add more prints to remind the user that the program is still working.

    for component in builds[0]:
        if not ((component == 'budget') | (component == 'request')):
            y[component] = []

    for idx, build in enumerate(builds):
        temp = []
        temp.append(float(build['budget']) / 1000)
        temp = temp + unroll(build['request'])
        x.append(temp)
        for component in build:
            if not ((component == 'budget') | (component == 'request')):
                y[component].append(build[component])

    filename_x = re.sub("_raw.json", "_trainx.json", filename)
    filename_y = re.sub("_raw.json", "_trainy.json", filename)
    x = json_encoder().encode(x)
    y = json_encoder().encode(y)
    with open(filename_x, "w") as text_file:
        text_file.write(x)
    with open(filename_y, "w") as text_file:
        text_file.write(y)
    print("Indexes for x (input) saved as: " + filename_x)
    print("Indexes for y (output) saved as: " + filename_y)
    return filename