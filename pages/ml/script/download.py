import praw
import json
import os.path
import datetime
from .function import gather_currency_rates
from .function import remove_questions
from .function import find_top_build
from .function import find_between
from .function import make_build
from .function import convert_to_usd
from .function import get_gigabyte
from .function import json_encoder

def download_from_reddit(limit = 99999, subreddit = "buildapcforme", length = 90):
    print('Reading Reddit configuration from config.json. If you have no idea about config.json, RTFM')

    with open("config.json", "r") as f:
        reddit = json.load(f)
        reddit = reddit['reddit']

    reddit = praw.Reddit(client_id=reddit['client_id'],
                         client_secret=reddit['client_secret'],
                         user_agent=reddit['user_agent'])
    builds = []

    print('Gathering currency rates online...')
    gather_currency_rates()
    print('Currency rates gathered')

    print("Downloading posts from Reddit. Please Wait.")

    max_age = datetime.timedelta(days=length)
    today = datetime.datetime.now()
    for submissionId in reddit.subreddit(subreddit).top("year", limit=limit):
        submission = reddit.submission(id=submissionId)
        created = submission.created
        created = datetime.datetime.fromtimestamp(created)
        age = today - created
        if max_age < age:
            print('Post skipped because its too old. Age: ' + str(age))
            continue
        comment = find_top_build(submission)

        if comment:
            request = remove_questions(submission.selftext)
            budget = find_between(comment, '**Total** | **', '**')
            if budget:
                region = find_between(comment, '[PCPartPicker part list](https://',
                                       '.pcpartpicker.com/')  # finds the PCPP region, could be 'ca' or 'uk', etc.
                budget = int(convert_to_usd(budget, region))
            CPU = find_between(comment, '**CPU** | [', '](')
            RAM = find_between(comment, '**Memory** | [', '](')
            RAM = find_between(RAM, 'GB (', ') DDR4-')
            HDD = find_between(comment, '**Storage** | [', ' Internal Hard Drive](') + 'HDD'
            if (not not (find_between(HDD, '**Storage** | [', 'HDD'))):
                HDD = find_between(HDD, '**Storage** | [', 'HDD')
            HDD = get_gigabyte(HDD)
            SSD = find_between(comment, '**Storage** | [', ' Solid State Drive](')
            SSD = get_gigabyte(SSD)
            GPU = find_between(comment, '**Video Card** | [', '](')
            if (GPU):
                GPU = find_between(GPU, ' - ', 'GB ') + 'GB'
            else:
                GPU = "No GPU"
            build = make_build(CPU, RAM, SSD, HDD, GPU, budget, request)
            builds.append(build)

    print('Builds downloaded: ' + str(len(builds)))
    builds = json_encoder().encode(builds)
    subdirectory = "data"

    try:
        os.mkdir(subdirectory)
    except Exception:
        pass

    now = datetime.datetime.now()
    filename = str(now.strftime("%y%m%d")) + '_' + subreddit + '_' + str(length) + 'days'

    print('Saving to file: ' + filename + '_raw.json')
    print('File located in data directory')
    with open(os.path.join(subdirectory, filename + "_raw.json"), "w") as text_file:
        text_file.write(builds)
        text_file.close()

    return os.path.join(subdirectory, filename + "_raw.json")