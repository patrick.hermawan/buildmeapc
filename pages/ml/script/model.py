import tensorflow as tf


def create_model(x_length, index_length, nodes = [200], learning_rate = 0, optimizer = 'adam'):
    # Setting up the parameters: LR and Optimizer
    learning_rate = float(learning_rate)
    optimizer = optimizer.lower()
    if optimizer == "adam":
        optimizer = tf.keras.optimizers.Adam(lr=learning_rate)
    elif optimizer == "rmsprop":
        optimizer = tf.keras.optimizers.RMSprop(lr=learning_rate)
    else:
        optimizer = tf.keras.optimizers.Adam()

    # Start designing the layers...
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=(x_length,)))
    for node in nodes:
            model.add(tf.keras.layers.Dense(node, activation=tf.nn.relu))

    # The last layer and loss depends if its a regression or a classification
    if index_length == 1:
        model.add(tf.keras.layers.Dense(index_length))
        loss = 'mae'
        metrics = ['mae']
    else:
        model.add(tf.keras.layers.Dense(index_length, activation=tf.nn.sigmoid))
        loss='sparse_categorical_crossentropy'
        metrics = ['accuracy']

    model.compile(optimizer=optimizer,loss=loss,metrics=metrics)
    return model
