from .script.download import download_from_reddit
from .script.process import process_file
from .script.train import train
import os
import re
import json


def ask_download():
    question = "\nDownload? NOTE: This will also generate the training file. Would take approximately 15 minutes. (1/0): "
    answer = input(question)
    while (answer != "1") & (answer != "0"):
        answer = input(question)

    if answer == "0":
        return False
    if answer == "1":
        subreddit = input("\nWhich Subreddit? (press ENTER for buildapcforme): ")
        length = input("\nHow far back? (in days, press ENTER for one month): ")
        limit = input("\nHow many posts to analyze? (press ENTER for unlimited): ")
        if (not subreddit):
            subreddit = "buildapcforme"
        if (not length):
            length = 90
        if (not limit):
            limit = 9999
        start = input("Start (1/0): ")
        while (start != "1") & (start != "0"):
            start = input("Start (1/0): ")
        if start == "1":
            print("======================")
            filename = download_from_reddit(int(limit), str(subreddit), int(length))
            return filename
        if start == "0":
            return ask_download()

def pick_file(question, ending):
    answer = input(question)
    while (answer != "1") & (answer != "0"):
        answer = input(question)

    if answer == "0":
        return False
    if answer == "1":
        print("\nEnumerating files in data directory. Please wait.")
        files = os.listdir('data')
        to_remove = []
        for file in files:
            if not(file[-(len(ending)):] == ending):
                to_remove.append(file)
        for file in to_remove:
            files.remove(file)
        print("\nFiles found:")
        for idx, file in enumerate(files):
            print('[' + str(idx) + '] ' + file)
        question2 = "\nEnter a number from 0 to " + str(len(files)-1) + ': '
        answer = input(question2)
        while (not re.sub("[^0-9]", "", answer)) | (not(0 <= int(re.sub("[^0-9]", "", answer)) <= (len(files)-1))) & (answer != "x"):
            answer = input(question2)
        if answer == 'x':
            return pick_file(question,ending)
        else:
            return os.path.join('data', files[int(answer)])

filename = ask_download()

if (not filename):
    filename = pick_file("\nProcess a RAW file into a TRAINING file? (1/0): ", '_raw.json')

if (filename):
    filename = process_file(filename)

if (not filename):
    filename = pick_file("\nTRAINING? Approximately 15 minutes with GPU, an eternity without. (1/0): ", '_trainx.json')

if (filename):
    question = "\nAre you sure want to do the TRAINING? (1/0): "
    answer = input(question)
    while (answer != "1") & (answer != "0"):
        answer = input(question)
    if answer == "0":
        filename = False
    if answer == "1":
        with open(re.sub("_trainx.json", "_index.json", filename), 'r') as filehandle:
            index = json.load(filehandle)
        for component in index:
            if index[component]:
                train(filename, component)