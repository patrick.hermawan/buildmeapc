from .script.predict import predict
import os

def predictx(budget, request):
    data_folder = os.path.join("pages","ml","data")
    files = os.listdir(data_folder)
    to_remove = []
    for file in files:
        if 'index' not in file:
            to_remove.append(file)
    for file in to_remove:
        files.remove(file)

    return predict(files[-1], budget, request)
