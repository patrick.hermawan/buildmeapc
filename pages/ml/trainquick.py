from .script.train import train
import os
import re

def is_input_number_valid(answer, max, min = 0):
    answer = (re.sub("[^0-9]", "", answer))
    try:
        if not answer:
            return False
        if min <= int(answer) <= max:
            return True
        return False
    except:
        return False

def pick_file(question, ending):
    print("Enumerating files in data directory. Please wait.")
    files = os.listdir('data')
    to_remove = []
    for file in files:
        if not(file[-(len(ending)):] == ending):
            to_remove.append(file)
    for file in to_remove:
        files.remove(file)
    print("Files found:")
    for idx, file in enumerate(files):
        print('[' + str(idx) + '] ' + file)
    question2 = "Enter a number from 0 to " + str(len(files)-1) + ': '
    answer = input(question2)
    answer = (re.sub("[^0-9]", "", answer))
    while  (not is_input_number_valid(answer,(len(files) - 1))) & (answer != "x"):
        answer = input(question2)
        answer = (re.sub("[^0-9]", "", answer))
    if answer == 'x':
        return pick_file(question,ending)
    else:
        return os.path.join('data', files[int(answer)])

filename = pick_file("TRAINING? Approximately 15 minutes with GPU, an eternity without. (1/0): ", '_trainx.json')

def repeated_train(filename, component):
    try:
        train(filename, component)
    except Exception as e:
        print(e)
        repeated_train(filename, component)

repeated_train(filename, "SSD")