from django.shortcuts import render
from .ml.predict import predictx
import json
# Create your views here.

def home(request):
    return render(request, "home.html", {})

def about(request):
    return render(request, "about.html", {})

def results(request):
    if request.method == 'POST':
        desc = request.POST['desc']
        budget = request.POST['budget']

        answer = predictx(budget,desc)
        answer = json.loads(answer)
        answer = answer[0]
        # import subprocess
        # output = subprocess.check_output(["script.py", "--", request.POST['hiya'])
        return render(request, "results.html",
                      {"answer": answer,
                       "second_name": answer})
    return render(request, "results.html", {})