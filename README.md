# Build Me a PC

_Say what now?_

_If you are expecting for this guide to be about building a robot that can manufacture a PC for you, then you can rest assured that you'll be disappointed._

![Random dude in front of a PC](https://cdn.fstoppers.com/styles/large-16-9/s3/lead/2017/05/best_pc_build_for_adobe_premiere.jpg)
*Here's some random picture of a confused guy next to a  PC*

The main purpose of this project is to give you a recommendation on which PC parts to choose 
(e.g. Intel Core i5 CPU) based on your description on how you are going to use the PC and your budget.

# What does it *exactly* do?

1. Scrapes reddit.com/r/buildapcforme (customizable for other subreddits)
2. Take the top comment with the build component (e.g. GPU) as the output (Y), and the total price of the build as *part* of the input (X)
3. Take the OP, remove the template questions (if exist), and convert it into a Word2Vec model, and take it to complete the input (X)
4. Train a TensorFlow model using the input and output
5. And now you can start entering your budget and description
6. The system spits out the recommended CPU, GPU, RAM, SSD, and HDD

*(Note I don't include the recommendation for motherboard, cooler, and PSU, as they are better off (in my opinion) 
with a simpler algorithm instead of AI/ML)*

# How accurate is this?

This figure will change over time, and likely this hasn't been updated with the latest achievements, but 
at the time of the writing it's able to identify 30-40% out of 30-ish options, so that's already 10 times 
of simply randomly selecting the parts.

I am hoping for better accuracy for SSD and HDD in particular, since it uses a *regression* instead of *classification* type

# How do I install?

First of all, get the following software.

## Prerequisites

* [Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html) ![Anaconda Nicki Minaj](https://beats4la.com/wp-content/uploads/2014/09/Nicki-Minaj-Anaconda.png)
* (recommended) [PyCharm](https://www.jetbrains.com/pycharm/download/)
* (recommended) [Git](https://git-scm.com/downloads)
* (recommended) Latest version of [Windows 10](https://www.microsoft.com/en-us/software-download/windows10)
* (recommended) [CUDA-enabled graphics card](https://developer.nvidia.com/cuda-gpus)

## Installation


####1. Setting up CUDA

*If you don't have a [CUDA-enabled graphics card](https://developer.nvidia.com/cuda-gpus), you can skip this section*

1. Download the [CUDA Toolkit 9.0](https://developer.nvidia.com/cuda-90-download-archive)
2. Install all the packages, there should be 4 files for the Windows version
3. Download the [CUDNN package](https://developer.nvidia.com/rdp/cudnn-download) for the CUDA version that you have (9.0)
4. Paste the contents of the CUDNN package to **C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0**, each folder (e.g bin, include, lib) has to match the existing folders
5. Ensure that you have the following in your [environment variables](https://www.tenforums.com/tutorials/121664-set-new-user-system-environment-variables-windows.html):
    * Variable Name: CUDA_PATH 
    * Variable Value: C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0

####2. Downloading the package

1. Download the BuildMeAPC package
    * (recommended) Using Git:
        1. Install [Git from the official website](https://git-scm.com/downloads)
        2. Open Git Bash
        3. Decide which folder you want this **project's folder in**. I will use **D:\Git_Projects** in this example
        4. Type **cd D:/Git_Projects**
        5. Type **git clone git@gitlab.com:ngakaks/buildmeapc.git**
        6. The project will be placed in D:/Git_Projects/BuildMeAPC
    * (not recommended) Without Git:
        *  Download the project as ZIP or other format, extract it
        
####3. Preparing the environment
1. Download and install [Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html) 
7. Open Anaconda Prompt
9. Move to the directory of the project by typing **cd /d D:/Git_Projects/BuildMeAPC**
8. Install all the necessary components by typing **conda env create -f environment.yaml**
9. Conda will output the environment path, save it. In this example it would be **C:\Users\\%userprofile%\\.conda\envs\buildmeapc**
10. Activate the environment by typing **conda activate buildmeapc**
11. Download the Spacy English resources by typing **python -m spacy download en**

####4. Setting up PyCharm (optional)

1. Downoad and install [PyCharm](https://www.jetbrains.com/pycharm/download/)
13. Open PyCharm and click **File > Open**
14. Navigate to the root of the BuildMeAPC folder, in this case **D:\Git_Projects\buildmeapc**
15. Go to **File > Settings > Project Interpreter** and tap on the GEAR icon on the top right corner
16. On the left side, click on **Conda Environment**
17. Navigate to the Conda environment folder. In this example, it would be **C:\Users\\%userprofile%\\.conda\envs\buildmeapc**
18. Press OK and select the environment

####5. Setting up IntelliJ IDEA (optional)

IntelliJ IDEA is a paid app that covers the functionality of PyCharm, in addition to the web development features. If you work on both sides of this project (ML and web), we would recommend to use this app.

TODO

####6. Setting up Sourcetree (optional)

1. Downoad and install [SourceTree](https://www.sourcetreeapp.com/)
13. Click on **Add** on the menu bar, the one with the folder icon
14. Navigate to the root of the BuildMeAPC folder, in this case **D:\Git_Projects\buildmeapc**
3. Click **Add**
15. On your browser, go to your [GitLab Personal Access Tokens](https://gitlab.com/profile/personal_access_tokens)
3. Generate and copy the token
4. Go back to SourceTree, go to Tools > Options > Authentication > Add
5. Choose **GitLab** as the Hosting Service
6. Cick on **Refresh Personal Access Token**
7. Type your username and use the copied token as password



# Usage

## Git pull

We recommend to do **git pull** regularly as often as possible. Git GUI programs could do that automatically or with a press of a button, or you could also use the command line / terminal.

## Launch the local server

1. Open Anaconda Prompt
2. Activate the environment by typing **conda activate buildmeapc**
9. Move to the directory of the project by typing **cd /d D:/Git_Projects/BuildMeAPC/**
4. Launch the server by typing **python manage.py runserver**

## Prediction using the console

9. Move to the directory of the ML folder **cd /d D:/Git_Projects/BuildMeAPC/pages/ml/**
8. Launch the prediction by typing **python predict_console.py**
3. Enter your budget in USD
4. Describe what you are going to do with the PC
6. And voila you get the predicted

## Downloading and Training using the console

1. Open Anaconda Prompt
9. Move to the directory of the project by typing **cd /d D:/Git_Projects/BuildMeAPC/pages/ml/**
8. Type **python prepare_console.py**
4. Follow the instructions on-screen


#TODO, setup mysql database:
1. \sql to swtich from JS mode to SQL mode
2. \connect root@localhost
3. create database buildmeapc;
4. Setup IntelliJ IDEA
5. File, PRoject structure
6. SDK
7. Conda, existing C:\Users\Patrick\AppData\Local\conda\conda\envs\django_learn
8. Project structure again
9. Modules
10. Choose the one you already chose

https://stackoverflow.com/questions/24438244/intellij-idea-not-display-django-in-the-facet-list

https://github.com/michaelbukachi/django-vuejs-tutorial/wiki/Django-Vue.js-Integration-Tutorial

https://medium.com/@fastlane80/setup-react-js-with-npm-babel-6-and-webpack-in-under-1-hour-1a714f973506


# FAQ (Frequently Asked Questions)

**Q: What's the language and framework used in this project?**

A: Language: ML side is pure Python. Web side is Python, HTML, JavaScript, and CSS. And English.
When it comes to framework, we use Django (web backend) and TensorFlow (ML), along with Praw for scraping Reddit, and Spacy, NLTK for processing natural language.

**Q: Where is the training done?**

A: Training could be done on the server, or an admin can also upload finished training model and weights (JSON model + H5 weights).

**Q: What's the database system used?**

A: SQLite for dev environment.

**Q: Is it possible to use older training data?**

A: Yes and no. It always defaults to use the newest data available, but you can change always change the date.

**Q: Will this be open source?**

A: Always, forever.

**Q: Can it scrape other website other than Reddit?**

A: Not at the moment. Let me know if there is a great website for this purpose.

**Q: Any particular Git GUI app you would recommend?**

A: I personally like Sourcetree.

**Q: Any particular CUDA GPU you would recommend?**

A: Anything with Pascal or Turing architecture (GTX 10 series or RTX 20 series).

